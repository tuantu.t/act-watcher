namespace ActWatcher.Config
{
    public class WorldTextConfig
    {
        public string AccountID { get; set; }
        public string ApiKey { get; set; }
    }
}