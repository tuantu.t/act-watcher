namespace ActWatcher.Core
{
    public class NexmoConfig
    {
        public string ApiSecret { get; set; }
        public string ApiKey { get; set; }
    }
}