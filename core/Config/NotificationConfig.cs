namespace ActWatcher.Config
{
    public class NotificationConfig
    {
        public string Recipient { get; set; }
        public bool NotifyStartup { get; set; } = false;
    }
}