using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ActWatcher.Config;
using Microsoft.Extensions.Logging;

namespace ActWatcher.Core
{
    public class NotifierService : INotifierService
    {
        private readonly ISmsService _smsService;
        private readonly NotificationConfig _config;
        private readonly ILogger<NotifierService> _logger;

        public NotifierService(
            ISmsService smsService,
            NotificationConfig config,
            ILogger<NotifierService> logger
        )
        {
            this._smsService = smsService;
            this._config = config;
            this._logger = logger;
        }

        public void NotifyNewOpenSlots(List<DateTime> newSlots)
        {
            if (String.IsNullOrEmpty(_config.Recipient)) throw new ApplicationException("config recipient is not set");

            using (var message = new StringWriter())
            {
                message.WriteLine("{0} new open slots:", newSlots.Count);
                newSlots.Sort();
                var byDay = newSlots.GroupBy(s => s.Date).OrderBy(g => g.Key);
                foreach (var day in byDay)
                {
                    var timeslots = day.Select(datetime => datetime.ToString("HH:mm"));
                    message.WriteLine("{0} {1}", day.Key.ToString("ddd d/M"), String.Join(", ", timeslots));
                }
                _smsService.SendSms(_config.Recipient, message.ToString());
            }
        }

        public void NotifyStartup()
        {
            if (_config.NotifyStartup)
            {
                _logger.LogInformation("notifying startup");
                if (String.IsNullOrEmpty(_config.Recipient)) throw new ApplicationException("config recipient is not set");
                _smsService.SendSms(_config.Recipient, "Service started on "+DateTime.Now);
            }
            else
            {
                _logger.LogInformation("skipped startup notification");
            }
        }
    }
}