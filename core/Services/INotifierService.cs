using System;
using System.Collections.Generic;

namespace ActWatcher.Core
{
    public interface INotifierService
    {
        void NotifyNewOpenSlots(List<DateTime> newSlots);
        void NotifyStartup();
    }
}