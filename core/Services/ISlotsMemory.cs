using System;
using System.Collections.Generic;

namespace ActWatcher.Core
{
    public interface ISlotsMemory
    {
        List<DateTime> GetLastRememberedSlots();
        void RememberSlots(List<DateTime> slots);
    }
}