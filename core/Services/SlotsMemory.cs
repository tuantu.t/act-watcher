using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace ActWatcher.Core
{
    public class SlotsMemory : ISlotsMemory
    {
        readonly string _jsonPath = "last_slots.json";
        public string Path { get => _jsonPath; }
        private readonly ILogger<SlotsMemory> _logger;
        List<DateTime> _slots = null;

        public SlotsMemory(ILogger<SlotsMemory> logger)
        {
            this._logger = logger;
        }
        public List<DateTime> GetLastRememberedSlots()
        {
            if (_slots == null && File.Exists(_jsonPath))
            {
                _logger.LogTrace("no slots in memory and file exists, read from {0} (pwd = {1})", _jsonPath, Environment.CurrentDirectory);
                var json = File.ReadAllText(_jsonPath);
                try
                {
                    _slots = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DateTime>>(json);
                }
                catch (Exception ex)
                {
                    _logger.LogError("Error while deserializing json: {0}", ex);
                    _logger.LogDebug("bad json:\n{0}", json);
                    _slots = null;
                }
            }
            return _slots?.ToList();
        }

        public void RememberSlots(List<DateTime> slots)
        {
            _slots = slots.ToList();
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(slots);
            File.WriteAllText(_jsonPath, json);
        }
    }
}