using System;
using System.Collections.Generic;

namespace ActWatcher.Core
{
    public interface ISlotsManager
    {
        List<DateTime> GetOpenSlots();
    }
}