using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Web;
using ActWatcher.Config;
using Microsoft.Extensions.Logging;

namespace ActWatcher.Core
{
    public class NexmoSmsService : ISmsService
    {
        private readonly NexmoConfig _config;
        private readonly ILogger<NexmoSmsService> _logger;
        readonly HttpClient _http;

        public NexmoSmsService(NexmoConfig config, ILogger<NexmoSmsService> logger)
        {
            this._config = config;
            this._logger = logger;
            _http = new HttpClient()
            {
                BaseAddress = new Uri("https://rest.nexmo.com/sms/json")
            };
        }
        public void SendSms(string recipient, string message)
        {
            if (string.IsNullOrEmpty(_config.ApiKey)) throw new ApplicationException("ApiKey is not set");
            if (string.IsNullOrEmpty(_config.ApiSecret)) throw new ApplicationException("ApiSecret is not set");

            var response = _http.PostAsync("", new FormUrlEncodedContent(new Dictionary<string, string>()
            {
                ["api_key"] = _config.ApiKey,
                ["api_secret"] = _config.ApiSecret,
                ["to"] = recipient,
                ["text"] = message,
                ["from"] = "Vonage APIs",
            })).Result;

            response.EnsureSuccessStatusCode();
            var body = response.Content.ReadAsStringAsync().Result;
            _logger.LogDebug(body);
        }
    }
}