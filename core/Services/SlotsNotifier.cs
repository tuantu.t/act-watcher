using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace ActWatcher.Core
{
    public class SlotsNotifier
    {
        private readonly ISlotsManager _slotsManager;
        private readonly ISlotsMemory _slotsMemory;
        private readonly INotifierService _notifier;
        private readonly ILogger<SlotsNotifier> _logger;

        public SlotsNotifier(
            ISlotsManager slotsManager,
            ISlotsMemory slotsMemory,
            INotifierService notifier,
            ILogger<SlotsNotifier> logger
        )
        {
            this._slotsManager = slotsManager;
            this._slotsMemory = slotsMemory;
            this._notifier = notifier;
            this._logger = logger;
        }

        public void CheckAndNotifySlots()
        {
            _logger.LogTrace("{0} : start", nameof(CheckAndNotifySlots));
            List<DateTime> currentSlots = _slotsManager.GetOpenSlots();

            currentSlots.RemoveAll(s => s.Date >= new DateTime(2020, 7, 28)); //we've already booked a slot for that date, we're only interested in earlier slots
            currentSlots.RemoveAll(s => s.Date <= new DateTime(2020, 7, 20, 9, 0, 0)); //we've already booked a slot for that date, we're only interested in earlier slots

            List<DateTime> lastSlots = _slotsMemory.GetLastRememberedSlots() ?? new List<DateTime>();

            var newSlots = currentSlots.Except(lastSlots).ToList();

            if (newSlots.Count > 0)
            {
                _logger.LogDebug("Notify new slots: {0}", String.Join(",", newSlots));
                _notifier.NotifyNewOpenSlots(newSlots);
            }
            else
            {
                _logger.LogDebug("no slots to notify");
            }

            _slotsMemory.RememberSlots(currentSlots);
        }
    }
}