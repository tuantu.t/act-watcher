namespace ActWatcher.Core
{
    public interface ISmsService
    {
        void SendSms(string recipient, string message);
    }
}