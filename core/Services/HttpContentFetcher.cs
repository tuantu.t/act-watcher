using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Xml;
using Microsoft.Extensions.Logging;

namespace ActWatcher.Core
{
    public class HttpContentFetcher : ISlotsManager
    {
        HttpClient _client;
        private const string __RequestVerificationToken = "__RequestVerificationToken";
        private readonly ILogger<HttpContentFetcher> _logger;

        public string ExamType { get; set; } = "Cat. B";

        public HttpContentFetcher(ILogger<HttpContentFetcher> logger)
        {
            var cookieJar = new CookieContainer();
            var httpHandler = new HttpClientHandler()
            {
                CookieContainer = cookieJar
            };
            _client = new HttpClient(httpHandler)
            {
                BaseAddress = new Uri("https://examen.autocontrole.be/")
            };
            this._logger = logger;
        }

        public List<DateTime> GetOpenSlots()
        {
            var home = GetHome(); //to initiate the cookie
            _logger.LogTrace("home page: {0}", home);
            if(home.Contains("Temporairement il n’y a plus de places disponibles. Le planning sera r&#233;ouvert 2 semaines avant la prochaine date disponible."))
            {
                return new List<DateTime>();
            }
            var selectExamPage = CheckId(home);
            _logger.LogTrace("selectExamPage:\r\n{0}", selectExamPage);
            if (HasOpenSlots(selectExamPage, out var token, out var idToPost))
            {
                _logger.LogTrace("found for with token {0} and id {1}", token, idToPost);
                var selectSlotPage = SubmitSelectExamType(token, idToPost);
                _logger.LogTrace("got selectSlotPage:\r\n{0}", selectSlotPage);
                List<DateTime> slots = ParseSlots(selectSlotPage);
                return slots;
            }
            else
            {
                return new List<DateTime>();
            }
        }

        private List<DateTime> ParseSlots(string selectSlotPage)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(selectSlotPage);
            var result = new List<DateTime>();
            var inputs = doc.DocumentNode.SelectNodes("//input[@timeslotid]");
            foreach (var input in inputs)
            {
                var dateSpan = input.SelectSingleNode("ancestor::div[contains(@class,'card')]/descendant::div[@class='card-header']/descendant::span");
                if (dateSpan == null) throw new ApplicationException("could not find date for " + input);
                var timeValue = input.GetAttributeValue("value", null);
                var datetime = DateTime.ParseExact(dateSpan.InnerText+" "+timeValue, "dd-MM-yy HH:mm", CultureInfo.InvariantCulture);
                result.Add(datetime);
            }
            return result;
        }

        private string SubmitSelectExamType(string token, string idToPost)
        {
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("Id", idToPost),
                new KeyValuePair<string, string>(__RequestVerificationToken, token),
            };
            var content = new FormUrlEncodedContent(keyValues);
            HttpResponseMessage responseMessage = _client.PostAsync("/Examen/Home/SelectExamType", content).Result;
            return responseMessage.Content.ReadAsStringAsync().Result;
        }

        public string GetHome()
        {
            string result = _client.GetStringAsync("/Examen/Language/ChooseLanguage?culture=fr-BE&url=%2FExamen%2F").Result;
            return result;
        }

        public string CheckId(string home)
        {
            var token = GetRequestVerificationToken(home);
            var keyValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("ExpirationDate", "24/03/2022"),
                new KeyValuePair<string, string>(__RequestVerificationToken, token),
            };
            var content = new FormUrlEncodedContent(keyValues);
            HttpResponseMessage page = _client.PostAsync("/Examen/Home/CheckId", content).Result;
            return page.Content.ReadAsStringAsync().Result;
        }

        public string GetRequestVerificationToken(string home)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(home);
            var input = doc.DocumentNode.SelectSingleNode($"//input[@name='{__RequestVerificationToken}']");
            if (input == null)
                throw new ApplicationException("Not found");
            return input.GetAttributeValue("value", string.Empty);
        }

        public bool HasOpenSlots(string selectPage, out string requestVerificationToken, out string idToPost)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(selectPage);
            var h6catB = doc.DocumentNode.SelectSingleNode($"//h6[text()='{ExamType}']");
            if (h6catB == null)
                throw new ApplicationException("Could not find exam type for " + ExamType);
            var form = h6catB.SelectSingleNode("ancestor::form");
            bool canSubmitForm = form.SelectSingleNode("descendant::input[@type='submit']") != null;
            if (canSubmitForm)
            {
                requestVerificationToken = form.SelectSingleNode($"descendant::input[@name='{__RequestVerificationToken}']")?.GetAttributeValue("value", null);
                if (requestVerificationToken == null) throw new ApplicationException("could not find requestVerificationToken");

                idToPost = form.SelectSingleNode("descendant::input[@name='Id']")?.GetAttributeValue("value", null);
                if (idToPost == null) throw new ApplicationException("could not find idToPost");

                return true;
            }
            else
            {
                requestVerificationToken = idToPost = null;
                return false;
            }
        }

    }
}