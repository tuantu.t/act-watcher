using System;
using System.Collections.Specialized;
using System.Net.Http;
using System.Web;
using ActWatcher.Config;

namespace ActWatcher.Core
{
    public class WorldTextSmsService : ISmsService
    {
        private readonly WorldTextConfig _config;
        private HttpClient _client;

        public WorldTextSmsService(WorldTextConfig config)
        {
            this._config = config;
            _client = new HttpClient();
        }
        public void SendSms(string recipient, string message)
        {
            if(String.IsNullOrEmpty(recipient)) throw new ArgumentException("recipient cannot be empty");
            Uri uri = BuildUri(recipient, message);
            var response = _client.PutAsync(uri, null).Result;
            response.EnsureSuccessStatusCode();
        }

        public Uri BuildUri(string recipient, string message)
        {
            NameValueCollection parameters = HttpUtility.ParseQueryString("");
            parameters.Add("id", _config.AccountID);
            parameters.Add("key", _config.ApiKey);
            parameters.Add("dstaddr", recipient.TrimStart('+'));
            parameters.Add("txt", message);
            var uriBuilder = new UriBuilder("https://sms.world-text.com/v2.0/sms/send");
            uriBuilder.Query = parameters.ToString();
            var uri = uriBuilder.Uri;
            return uri;
        }
    }
}