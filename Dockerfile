FROM mcr.microsoft.com/dotnet/core/runtime:3.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY core/core.csproj core/
COPY watcher-service/watcher-service.csproj watcher-service/
RUN dotnet restore watcher-service/watcher-service.csproj
COPY . .
WORKDIR /src/
RUN dotnet build watcher-service/watcher-service.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish watcher-service/watcher-service.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "watcher-service.dll"]