using System;
using System.Collections.Generic;
using System.IO;
using ActWatcher.Config;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace ActWatcher.Core.Tests
{
    [TestFixture]
    public class TestNotifierService
    {
        [Test]
        public void ItCanNotifyOpenSlots()
        {
            var smsService = new Mock<ISmsService>(Moq.MockBehavior.Strict);
            var calls = new List<(string,string)>();
            smsService.Setup(sms => sms.SendSms(It.IsAny<string>(), It.IsAny<string>())).Callback((string recipient, string message) => {
                calls.Add((recipient, message));
            });
            var notifier = new NotifierService(smsService.Object, new NotificationConfig()
            {
                Recipient = "foobar"
            }, null);

            var newOpenSlots= new List<DateTime>()
            {
                new DateTime(2020,7,20,14,15,0),
                new DateTime(2020,7,20,14,17,0),
                new DateTime(2020,7,20,15,19,0),
                new DateTime(2020,7,20,15,20,0),
                new DateTime(2020,7,20,15,21,0),
                new DateTime(2020,7,21,14,18,0),
            };

            notifier.NotifyNewOpenSlots(newOpenSlots);

            Assert.That(calls.Count, Is.EqualTo(1),"calls count");
            var (dst, msg) = calls[0];
            Assert.That(dst, Is.EqualTo("foobar"), "recipient");
            var expected = EmbeddedResourceHelper.EmbeddedResource.GetAsStringFromCallingAssemly("expectedSms.txt").Trim();
            Console.WriteLine(msg);
            Assert.That(msg.Trim(), Is.EqualTo(expected),"msg");
        }

    }
}