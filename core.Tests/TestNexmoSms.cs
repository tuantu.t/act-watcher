using System;
using ActWatcher.Config;
using NUnit.Framework;

namespace ActWatcher.Core.Tests
{
    public class TestNexmoSms
    {
        [Test]
        public void ItCanSendAnSms()
        {
            //Fill these for the test
            var number = "";
            var apiKey = "";
            var apiSecret = "";

            NexmoConfig config;
            var service = new NexmoSmsService(config = new NexmoConfig()
            {
                ApiKey = apiKey,
                ApiSecret = apiSecret,
            }, LoggerHelper.GetLogger<NexmoSmsService>());

            var msg = "It worked on " + DateTime.Now + "!";

            Assume.That(config.ApiKey, Is.Not.Empty, "api key");
            Assume.That(config.ApiSecret, Is.Not.Empty, "api secret");
            Assume.That(number, Is.Not.Empty, "number");

            service.SendSms(number, msg);
        }
    }
}
