using Microsoft.Extensions.Logging;

namespace ActWatcher.Core.Tests
{
    static class LoggerHelper
    {
        public static ILogger<T> GetLogger<T>()
        {
            var factory = LoggerFactory.Create(builder => {
                builder.AddConsole();
                builder.SetMinimumLevel(LogLevel.Trace);
            });
            return factory.CreateLogger<T>();
        }

    }
}