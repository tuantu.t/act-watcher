using System;
using ActWatcher.Config;
using NUnit.Framework;

namespace ActWatcher.Core.Tests
{
    public class TestWorldTextSms
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ItCanBuildTheUrl()
        {
            var smsService = new WorldTextSmsService(new WorldTextConfig()
            {
                AccountID="accId",
                ApiKey = "apiKey"
            });

            var uri = smsService.BuildUri("+321234123123","hello world");
            Console.WriteLine(uri);
            Assert.That(uri.ToString(), Is.EqualTo("https://sms.world-text.com/v2.0/sms/send?id=accId&key=apiKey&dstaddr=321234123123&txt=hello+world"));
        }

        [Test]
        public void ItCanSendAnSms()
        {
            //fill info
            var recipient = "+321234123123";
            var msg = "It works!";
            var id = "";
            var key = "";

            Assume.That(id, Is.Not.Empty, "empty id");
            Assume.That(key, Is.Not.Empty, "empty key");
            Assume.That(recipient, Is.Not.Empty, "empty recipient");

            var smsService = new WorldTextSmsService(new WorldTextConfig()
            {
                AccountID=id,
                ApiKey = key
            });

            smsService.SendSms(recipient, msg);
        }
    }
}