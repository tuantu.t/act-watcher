using System;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace ActWatcher.Core.Tests
{
    public class TestHttpContentFetcher
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var factory = LoggerFactory.Create(builder => {
                builder.AddConsole();
                builder.SetMinimumLevel(LogLevel.Trace);
            });
            var fetcher = new HttpContentFetcher(factory.CreateLogger<HttpContentFetcher>())
            {
                // ExamType = "Cat. D - avec CAP"
            }
            ;
            var slots = fetcher.GetOpenSlots();
            Console.WriteLine("{0} slots", slots.Count);
            Console.WriteLine(String.Join(",", slots));
        }
    }
}