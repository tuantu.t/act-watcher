using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Logging;
using NUnit.Framework;

namespace ActWatcher.Core.Tests
{
    public class TestSlotsMemory
    {
        [Test]
        public void ItCanRememberSlots()
        {
            var memory = new SlotsMemory(LoggerHelper.GetLogger<SlotsMemory>());
            if(File.Exists(memory.Path))
            {
                Console.WriteLine("Delete {0}", memory.Path);
                File.Delete(memory.Path);
            }
            List<DateTime> slots = memory.GetLastRememberedSlots();
            Assert.That(slots, Is.Null, "uninitialized memory should return null");

            var expected = slots = new List<DateTime>()
            {
                DateTime.Today,
                DateTime.Now,
                DateTime.Now.AddDays(1),
            };

            memory.RememberSlots(slots);
            slots = memory.GetLastRememberedSlots();
            Assert.That(slots, Is.EqualTo(expected), "after 1st remember slots");

            memory = new SlotsMemory(LoggerHelper.GetLogger<SlotsMemory>());
            slots = memory.GetLastRememberedSlots();
            Assert.That(slots, Is.EqualTo(expected), "after re-init");
        }
    }
}