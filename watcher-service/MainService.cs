using System;
using System.Threading;
using System.Threading.Tasks;
using ActWatcher.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace watcher_service
{
    internal class MainService : IHostedService
    {
        public MainService(
            SlotsNotifier slotsNotifier,
            INotifierService notifierService,
            IHostApplicationLifetime hostApplicationLifetime,
            ILogger<MainService> logger
        )
        {
            this._slotsNotifier = slotsNotifier;
            this._notifierService = notifierService;
            this._hostApplicationLifetime = hostApplicationLifetime;
            this._logger = logger;
        }
        Timer _timer;
        private const int _PERIOD_MS = 60_000;
        private readonly SlotsNotifier _slotsNotifier;
        private readonly INotifierService _notifierService;
        private readonly IHostApplicationLifetime _hostApplicationLifetime;
        private readonly ILogger<MainService> _logger;

        public Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                _notifierService.NotifyStartup();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error while notifying startup: " + ex);
                _hostApplicationLifetime.StopApplication();
            }
            _timer = new Timer(TimerElapsed, null, 0, Timeout.Infinite);
            return Task.CompletedTask;
        }

        private void TimerElapsed(object state)
        {
            try
            {
                _slotsNotifier.CheckAndNotifySlots();
            }
            catch (System.Exception ex)
            {
                _logger.LogError("Uncaught error: " + ex);
            }
            finally
            {
                _timer.Change(_PERIOD_MS, Timeout.Infinite);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            return Task.CompletedTask;
        }
    }
}