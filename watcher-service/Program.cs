﻿using System;
using ActWatcher.Config;
using ActWatcher.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace watcher_service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(config =>
                {
                    config.Sources.Clear();
                    config.AddXmlFile("config.xml", optional: true);
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.AddNLog();
                    logging.SetMinimumLevel(LogLevel.Trace);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSingleton<NotificationConfig>(serviceProvide =>
                    {
                        var config = new NotificationConfig();
                        hostContext.Configuration.GetSection("NotificationConfig").Bind(config);
                        return config;
                    });
                    services.AddSingleton<NexmoConfig>(serviceProvide =>
                    {
                        var config = new NexmoConfig();
                        hostContext.Configuration.GetSection("NexmoConfig").Bind(config);
                        return config;
                    });
                    services.AddSingleton<ISmsService, NexmoSmsService>();
                    services.AddSingleton<INotifierService, NotifierService>();
                    services.AddSingleton<ISlotsMemory, SlotsMemory>();
                    services.AddSingleton<SlotsNotifier>();
                    services.AddSingleton<ISlotsManager, HttpContentFetcher>();
                    services.AddHostedService<MainService>();
                });
    }
}
